<?xml version="1.0"?>
<Definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <PlanetGeneratorDefinitions>
    <PlanetGeneratorDefinition>
        <Id>
          <TypeId>PlanetGeneratorDefinition</TypeId>
          <SubtypeId>Ravcor</SubtypeId>
        </Id>
        <PlanetMaps Material="true" Ores="true" Biome="false" Occlusion="true"/>

        <SurfaceDetail>
          <Texture>Data/PlanetDataFiles/Extra/material_detail_1</Texture>
          <Size>384</Size>
          <Scale>5</Scale>
          <Slope Min="6" Max="90" />
          <Transition>7.5</Transition>
        </SurfaceDetail>


	    <GravityFalloffPower>4.47</GravityFalloffPower>
	    <SurfaceGravity>5.0</SurfaceGravity>

	    <DistortionTable>
        <Distortion Type="Perlin" Value ="92" Frequency="10" Height="4" LayerCount="6"/>
      </DistortionTable>
  
      <OreMappings>
         
		      <Ore Value="210" Type="Radium_01" Start="230" Depth="30" />
        	<Ore Value="211" Type="Radium_02" Start="285" Depth="40"/>
		      <Ore Value="220" Type="Californium_01" Start="230" Depth="30"/>
		      <Ore Value="221" Type="Californium_02" Start="285" Depth="40"/>
		      <Ore Value="230" Type="Diamond_01" Start="210" Depth="30"/>
        	<Ore Value="231" Type="Diamond_02" Start="300" Depth="50" />
    	
          <!-- Old Ores
		      <Ore Value="1" Type="Iron_02" Start="210" Depth="30" />
          <Ore Value="24" Type="Flerow_01" Start="220" Depth="30"/>
		      <Ore Value="36" Type="Tau_01" Start="230" Depth="30"/>
		      <Ore Value="48" Type="Nihonium_01" Start="285" Depth="40"/>
		      <Ore Value="60" Type="Lorens_01" Start="300" Depth="50"/>
          -->
      </OreMappings>

      <SoundRules>
	    <SoundRule>
          <Height Min="0" Max="1"/>
          <Latitude Min="0" Max="90"/>
          <SunAngleFromZenith Min="0" Max="180"/>
          <EnvironmentSound>AmbMarsLoop</EnvironmentSound>
        </SoundRule>
      </SoundRules>
  
	    <MusicCategories>
        <MusicCategory Category="Planet" Frequency="0.7" />
        <MusicCategory Category="Danger" Frequency="0.6" />
      </MusicCategories>
  
        <ComplexMaterials>
          <MaterialGroup Name="Planet Features" Value="0">
  
		      <Rule>
              <Layers>
                <Layer Material="RavLava" Depth="18"/>
                <Layer Material="RavRock" Depth="8"/>
              </Layers>
              <Height Min="0" Max="0.0025"/>
              <Latitude Min="0" Max="90"/>
              <Slope Min="0" Max="8"/>
            </Rule>
  
	  	    <Rule>
              <Layers>
                <Layer Material="RavRock" Depth="4"/>
              </Layers>
              <Height Min="0" Max="0.0025"/>
              <Latitude Min="0" Max="90"/>
              <Slope Min="8" Max="24"/>
            </Rule>
  
	  	    <Rule>
              <Layers>
                <Layer Material="RavCrackedRock" Depth="28"/>
              </Layers>
              <Height Min="0" Max="0.0025"/>
              <Latitude Min="0" Max="90"/>
              <Slope Min="24" Max="90"/>
            </Rule>
  
	  	    <Rule>
              <Layers>
                <Layer Material="RavSoil" Depth="2"/>
                <Layer Material="RavRock" Depth="8"/>
              </Layers>
              <Height Min="0.0025" Max="1"/>
              <Latitude Min="0" Max="90"/>
              <Slope Min="0" Max="8"/>
            </Rule>
  
	  	    <Rule>
              <Layers>
                <Layer Material="RavRock" Depth="10"/>
              </Layers>
              <Height Min="0.0025" Max="1"/>
              <Latitude Min="0" Max="90"/>
              <Slope Min="8" Max="24"/>
            </Rule>
  
  		    <Rule>
              <Layers>
                <Layer Material="RavCrackedRock" Depth="8"/>
              </Layers>
              <Height Min="0.0025" Max="1"/>
              <Latitude Min="0" Max="90"/>
              <Slope Min="24" Max="90"/>
            </Rule>
  
          </MaterialGroup>
        </ComplexMaterials>

        <EnvironmentItems>
		      <Item>
            <Biomes>
              <Biome>0</Biome>
            </Biomes>
            <Materials>
              <Material>RavSoil</Material>
            </Materials>
            <Items>
            </Items>
            <Rule>
              <Height Min="0" Max="1.2"/>
              <Latitude Min="0" Max="90"/>
              <Slope Min="0" Max="90"/>
            </Rule>
          </Item>
        </EnvironmentItems>

        <DefaultSurfaceMaterial Material="RavLava" MaxDepth="50"/>
        <DefaultSubSurfaceMaterial Material="RavLava"/>

        <HasAtmosphere>true</HasAtmosphere>
	      <MaximumOxygen>0.1</MaximumOxygen>
	    <Atmosphere>
          <Breathable>false</Breathable>
		      <OxygenDensity>0</OxygenDensity>
		      <Density>1.54</Density>
		      <LimitAltitude>14.0</LimitAltitude>
          <MaxWindSpeed>40</MaxWindSpeed>
	    </Atmosphere>

      <AtmosphereSettings>
		    <RayleighScattering>
		      <X>11.888</X>
		      <Y>43.848</Y>
		      <Z>80</Z>
		    </RayleighScattering>
		    <MieScattering>50</MieScattering>
		    <MieColorScattering>
		      <X>16.614</X>
		      <Y>63.535</Y>
		      <Z>150</Z>
		    </MieColorScattering>
		    <RayleighHeight>24.085</RayleighHeight>
		    <RayleighHeightSpace>12.036</RayleighHeightSpace>
		    <RayleighTransitionModifier>0.4</RayleighTransitionModifier>
		    <MieHeight>200</MieHeight>
		    <MieG>0.9984</MieG>
		    <Intensity>0.269</Intensity>
		    <SeaLevelModifier>0.960</SeaLevelModifier>
		    <AtmosphereTopModifier>1.1</AtmosphereTopModifier>
		    <FogIntensity>1</FogIntensity>
		    <Scale>1</Scale>
      </AtmosphereSettings>
  
	    <DefaultSurfaceTemperature>ExtremeHot</DefaultSurfaceTemperature>
  
        <MaterialsMaxDepth Min="4000" Max="4000"/>
        <MaterialsMinDepth Min="20" Max="20"/>

        <HillParams Min="0.0" Max ="0.07"/>
  
    </PlanetGeneratorDefinition>
  </PlanetGeneratorDefinitions>
</Definitions>
